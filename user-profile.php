<?php
/**
 * Template Name: User Profile
 *
 * Allow users to update their profiles from Frontend.
 *
 */
/* Get user info. */
global $current_user, $wp_roles;

$error = array();
/* If profile was saved, update profile. */
if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {

    /* Update user password. */
    if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
        if ( $_POST['pass1'] == $_POST['pass2'] ) {
            wp_update_user(array('ID' => $current_user->ID, 'user_pass' => esc_attr($_POST['pass1'])));
        } else {
            $error[] = __('The passwords you entered do not match.  Your password was not updated.', 'profile');
        }
    }

    /* Update user information. */
    if ( !empty( $_POST['email'] ) ) {
        if (!is_email(esc_attr( $_POST['email'] ))) {
            $error[] = __('The Email you entered is not valid.  please try again.', 'profile');
        } elseif (email_exists(esc_attr( $_POST['email'] )) != $current_user->id ) {
            $error[] = __('This email is already used by another user.  try a different one.', 'profile');
        } else {
            wp_update_user( array ('ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['email'] )));
        }
    }

    if ( !empty( $_POST['first-name'] ) ) {
        update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first-name'] ) );
    }
    if ( !empty( $_POST['last-name'] ) ) {
        update_user_meta($current_user->ID, 'last_name', esc_attr( $_POST['last-name'] ) );
    }

    /* Redirect so the page will show updated info.*/
    if ( count($error) == 0 ) {
        //action hook for plugins and extra fields saving
        do_action('edit_user_profile_update', $current_user->ID);
        wp_redirect( get_permalink() .'?updated=true' );
        exit;
    }
}
?>

<?php get_header(); ?>

	
<!-- #Content -->
<div id="Content">
	<div class="content_wrapper clearfix">

		<!-- .sections_group -->
		<div class="sections_group">
		
			<div class="entry-content" itemprop="mainContentOfPage">


                <div class="section the_content has_content">
                    <div class="section_wrapper">
                        <div class="the_content_wrapper">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">



				<?php 
					while ( have_posts() ){
						the_post();							// Post Loop
						mfn_builder_print( get_the_ID() );	// Content Builder & WordPress Editor Content ?>

                        <?php if ( !is_user_logged_in() ) : ?>
                            <p class="warning">
                                <?php _e('You must be logged in to edit your profile.', 'profile'); ?>
                            </p><!-- .warning -->
                        <?php else : ?>

                            <?php global $current_user; ?>
                            <div class="profile-welcome-msg">
                                <h2 class="custom-h2-m rich-red uppercase">Welcome <?php echo $current_user->user_firstname; ?></h2>
                                <p>You can update or edit your information below.</p>
                            </div>

                            <?php if ( count($error) > 0 ) { ?><p class="msg-error"> <?php echo implode("<br />", $error); } ?></p>
                            <?php if ( $_GET['updated'] == 'true' ) { ?><p class="msg-success"><?php _e('Your profile has been updated', 'profile'); ?></p> <?php } ?>
                            <form method="post" id="adduser" action="<?php the_permalink(); ?>">
                                <div class="profile-table">
                                    <div class="profile-col">
                                        <h2 class="custom-h2-m dark-grey uppercase">Personal Information</h2>
                                        <p class="form-username">
                                            <label for="first-name"><?php _e('First Name', 'profile'); ?></label>
                                            <input class="text-input" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" />
                                        </p><!-- .form-username -->
                                        <p class="form-username">
                                            <label for="last-name"><?php _e('Last Name', 'profile'); ?></label>
                                            <input class="text-input" name="last-name" type="text" id="last-name" value="<?php the_author_meta( 'last_name', $current_user->ID ); ?>" />
                                        </p><!-- .form-username -->
                                        <p class="form-email">
                                            <label for="email"><?php _e('E-mail *', 'profile'); ?></label>
                                            <input class="text-input" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
                                        </p><!-- .form-email -->
                                    </div>
                                    <div class="profile-col">
                                        <h2 class="custom-h2-m dark-grey uppercase">Password</h2>
                                        <p class="form-password">
                                            <label for="pass1"><?php _e('Password *', 'profile'); ?> </label>
                                            <input class="text-input" name="pass1" type="password" id="pass1" />
                                        </p><!-- .form-password -->
                                        <p class="form-password">
                                            <label for="pass2"><?php _e('Repeat Password *', 'profile'); ?></label>
                                            <input class="text-input" name="pass2" type="password" id="pass2" />
                                        </p><!-- .form-password -->
                                        <div class="delete-my-account-row">
                                            <?php echo do_shortcode( '[plugin_delete_me /]' ); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-submit profile-form-actions-footer">
                                    <div class="profile-form-actions-footer-btn">
                                        <?php echo $referer; ?>
                                        <input name="updateuser" type="submit" id="updateuser" class="submit button" value="<?php _e('UPDATE YOUR PROFILE', 'profile'); ?>" />
                                        <?php wp_nonce_field( 'update-user' ) ?>
                                        <input name="action" type="hidden" id="action" value="update-user" />
                                    </div>
                                </div><!-- .form-submit -->
                            </form><!-- #adduser -->
                        <?php endif; ?>

					<?php } ?>
                                                </div>
                                            </div>
                                        </div></div></div></div>
                        </div></div></div>
				
			</div>
		</div>
	</div>
</div>

<?php get_footer();

// Omit Closing PHP Tags