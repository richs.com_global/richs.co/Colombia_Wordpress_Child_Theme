<div class="container">
    <div class="flex">
        <?php foreach ($block['items'] as $item) : ?>
            <div class="item">
                <div class="header">
                    <div class="value"><?= $item['title']; ?></div>
                </div>
                <div class="content">
                    <?php if ($label = $item['label']) : ?>
                        <div class="label"><?= $label; ?></div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="button-container">
        <?php if ($button = $block['button']) : ?>
            <p><a class="btn btn-ghost" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a></p>
        <?php endif; ?>
    </div>
</div>