<?php 

get_header();

$cat_id= get_queried_object()->term_id;
$cat_name= get_queried_object()->name;
$cat_slug= get_queried_object()->slug;
$categories = get_terms( 'product_categories', ['child_of' => $cat_id] );
$tags = get_the_terms( $id, 'post_tag');

$parent = get_term_parents_list( $categories[0]->term_id, 'product_categories', array('link' => false) );
$parentName = explode("/",$parent);

$args = array(
    'type' => 'product',
    'orderby' => 'name',
    'order' => 'ASC'
);
$tags = get_tags($args);

?>
<div class="product-breadcrumbs" id="crumb">
    <div class="container">
        <a href="<?php echo get_home_url(); ?>/productos">Productos</a>
        <?php if($parentName !== null) {
            ?>
            / 
            <a href="<?php echo get_home_url() . "/product_categories/" .  $parentName[0]; ?>"><?php echo $parentName[0] ?></a>
            <?php
        }
        ?>
    </div>
</div>


<div class="container category-template"> <?php

    if (!empty($categories))  { ?>
        <div class="category-page">
            <div class="category-sidebar">
                <div class="category-search"> <?php 
                    echo do_shortcode('[ivory-search id="5086" title="Product Search"]'); ?>
                </div> 
                <div class="section-separator"></div>
                <div class="category-subCategories">
                    <h3>Categories</h3> <?php
                    foreach ( $categories as $category ) { ?>
                        <h4><?php echo $category->name; ?></h4>
                        <?php 
                    } ?>
                </div>
                <div class="section-separator"></div>
                    
            </div> 
            <div class="category-content">
                <h1><?php echo $cat_name ?></h1>
                <?php
                foreach ( $categories as $category ) {  ?>
                    <div>
                        <h2><?php echo $category->name; ?></h2>
                        <div class="product-list"> <?php
                            $categoryArgs = array(
                                'post_type'   => 'product',
                                'post_status' => 'publish',
                                'posts_per_page' => -1,
                                'tax_query'   => array(
                                    array(
                                        'taxonomy' => 'product_categories',
                                        'field'    => 'slug',
                                        'terms'    => $category->slug
                                    )
                                )
                            );
                            $categoryProducts = new WP_Query( $categoryArgs );
                            while( $categoryProducts->have_posts() ) :
                                $categoryProducts->the_post();
                                ?>
                                <div class="product-list-item col-ie-md-4">
                                    <a href="<?php echo get_the_permalink(); ?>">
                                    <div class="image-container">
                                    <?php  $image = get_field( "Digital_Features_and_Benefits_1", get_the_ID());
                                        if ($image) { ?>
                                            <div class="image-container">
                                            <img src="<?php echo $image;  ?>" /> </div><?php
                                        } else { ?>
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/placeholder.jpg" alt='placeholder' />
                                        <?php
                                    } ?>
                                    </div>
                                        <h3 class="product-title"><?php echo get_the_title(); ?></h3>
                                        <?php $member_tags = get_the_terms( get_the_ID(), 'post_tag' ); 
                                        foreach ( $member_tags as $tag) { 
                                            if ($tag->name == "New") {?>
                                                <div class="product-tag">
                                                    <span><?php echo $tag->name; ?></span>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>     
                                    </a>
                                </div> 
                            <?php
                            endwhile;?>
                        </div> 
                                
                    </div> 
                <?php
                } ?>
            </div> 
        </div> 
    <?php
    } else { ?>
        <div class="category-page">
            <div class="category-sidebar">
                <div class="category-search">  <?php 
                    echo do_shortcode('[ivory-search id="5086" title="Product Search"]'); ?>
                </div> 
                <div class="section-separator"></div>
                <!-- <div class="category-subCategories">
                    <h3>Sub Categories</h3>
                    <a href="<?php echo get_home_url() . '/product_categories/' . $cat_slug ?>"><?php echo $cat_name ?></a>
                </div>
                <div class="section-separator"></div> -->
                <div class="category-tags">
                        <!-- <h3>Tags</h3> -->
                </div>
                <div class="category-subCategories">
                    <h3>CATEGORÍAS</h3>
                   
                    <?php
                        $taxonomy = 'product_categories';
                        $terms = get_terms($taxonomy); // Get all terms of a taxonomy

                        if ( $terms && !is_wp_error( $terms ) ) {
                        ?>
                            <div class="">
                                <?php foreach ( $terms as $term ) {
                                    if ($term->parent == 0) { 
                                        ?> 
                                        <div>
                                            <a href="<?php echo get_term_link($term->slug, $taxonomy); ?>">
                                            <?php echo $term->name; ?>
                                            </a>
                                        </div>
                                    <?php } else { 
                                        // Do nothing
                                    }
                            } ?>
                            </div>
                        <?php } else { 
                                $args = array(
                                'post_type'   => 'product',
                                'post_status' => 'publish',
                                'posts_per_page' => -1 
                                );
                                
                                $products = new WP_Query( $args );
                                if( $products->have_posts() ) :
                                ?>
                        
                                <div class="">
                                    <?php
                                    while( $products->have_posts() ) :
                                        $products->the_post();
                                        $title = get_the_title();
                                        if ($title !== "") {
                                        ?>
                                        <div class=""> 
                                            <a href="<?php the_permalink(); ?>">
                                            <h3><?php echo get_the_title(); ?></h3>
                   
                                            </a>
                                        </div>
                                        <?php
                                        }
                                    endwhile;
                                    wp_reset_postdata();
                                    ?>
                                </div>
                        
                                <?php
                                else :
                                esc_html_e( 'No products in the taxonomy!' );
                                endif;
                            } ?>
                            
                </div>
            </div> 
        <div class="category-content">
            <h1><?php echo $cat_name ?></h1> 
            <?php
            
            ?><div class="product-list"> <?php   
            $categoryArgs = array(
                'post_type'   => 'product',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'tax_query'   => array(
                    array(
                        'taxonomy' => 'product_categories',
                        'field'    => 'slug',
                        'terms'    => $cat_slug
                    )
                )
            );
            $categoryProducts = new WP_Query( $categoryArgs );
            while( $categoryProducts->have_posts() ) :
                $categoryProducts->the_post();
                ?>
                <div class="product-list-item col-ie-md-4">
                    <a href="<?php echo get_the_permalink(); ?>">
                        <div class="image-container">
                        <?php  $image = get_field( "Digital_Features_and_Benefits_1", get_the_ID());
                                        if ($image) { ?>
                                            <div class="image-container">
                                            <img src="<?php echo $image;  ?>" /> </div><?php
                                        }  else { ?>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/placeholder.jpg" alt='placeholder' />
                            <?php
                        } ?>
                        </div>
                        <h3 class="product-title"><?php echo get_the_title(); ?></h3>
                        <?php $member_tags = get_the_terms( get_the_ID(), 'post_tag' ); 
                        foreach ( $member_tags as $tag) { 
                            if ($tag->name == "New") {?>
                                <div class="product-tag">
                                    <span><?php echo $tag->name; ?></span>
                                </div>
                                <?php
                            }
                        }
                        ?>     
                    </a>
                </div> 
            <?php
            endwhile;?>
    </div>
    </div>
    <?php
    }
    ?>
</div>

<?php

get_footer();