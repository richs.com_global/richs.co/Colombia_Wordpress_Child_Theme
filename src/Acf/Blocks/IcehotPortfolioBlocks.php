<?php

namespace Ix\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class IcehotPortfolioBlocks extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/icehot-portfolio-blocks')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('icehot-portfolio-blocks');
        $fields
            ->addImage('background_image')
            ->addText('top_title')
            ->addText('title')
            ->addWysiwyg('copy')
            ->addRepeater('product_range_blocks')
                ->addColorPicker('background_color')
                ->addSelect('button_color', ['default_value' => "pink_btn", 'choices' =>
                [
                    ['pink_btn' => 'PinkBtn'],
                    ['white_btn' => 'WhiteBtn']
                ]])
                ->addText('product_title')
                ->addWysiwyg('product_copy')
                ->addImage('image')
                ->addLink('image_link')
                    ->addRepeater('featuring_flavours')
                        ->addImage('image_flavour')
                        ->addText('text_flavour')
                    ->endRepeater()
            ->endRepeater()
            ->addLink('button');
        return $fields;
    }
}

