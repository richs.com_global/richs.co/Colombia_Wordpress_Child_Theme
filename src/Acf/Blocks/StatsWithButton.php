<?php

namespace Ix\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class StatsWithButton extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/stats-with-button')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('highlights');
        $fields
            ->addText('title')
            ->addFields($this->getBackgroundColorSelect())
            ->addRepeater('items')
                ->addText('title')
                ->addTextArea('label')
            ->endRepeater()
            ->addLink('button');
        return $fields;
    }
}