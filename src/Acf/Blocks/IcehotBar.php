<?php

namespace Ix\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class IcehotBar extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/icehot-bar')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('icehot-bar');
        $fields
            ->addColorPicker('background_color')
            ->addImage('image');
        return $fields;
    }
}