<?php

namespace Ix\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class CommunityGrid extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/community-grid')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('highlights');
        $fields
            ->addText('title')
            ->addRelationship('organisations', ['post_type' => 'community']);
        return $fields;
    }
}