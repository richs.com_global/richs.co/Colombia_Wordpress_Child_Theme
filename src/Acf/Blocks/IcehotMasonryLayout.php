<?php

namespace Ix\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class IcehotMasonryLayout extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/icehot-masonry-layout')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('icehot-masonry-layout');
        $fields
            ->addText('top_title')
            ->addText('title')
            ->addWysiwyg('text')
            ->addRepeater('masonry_images')
                ->addImage('image')
                ->addLink('image_link')
            ->endRepeater()
            ->addLink('button');
        return $fields;
    }
}


