<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark
 * @version  $Revision: 1.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

class IcehotPortfolioRange extends Block
{
    public function __construct()
    {
        parent::register_block(
            'icehot-portfolio-range',
            [
                'title'           => 'Icehot Portfolio Range',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['text', 'image', 'range']
            ]
        );
    }
}