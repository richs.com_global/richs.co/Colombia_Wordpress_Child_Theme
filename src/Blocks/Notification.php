<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

class Notification extends Block
{
    public function __construct()
    {
        parent::register_block(
            'notification',
            [
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['notifications']
            ]
        );
    }
}