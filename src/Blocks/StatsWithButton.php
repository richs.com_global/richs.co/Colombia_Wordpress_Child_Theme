<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark Digital
 * @version  $Revision: 1.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

class StatsWithButton extends Block
{
    public function __construct()
    {
        parent::register_block(
            'stats-with-button',
            [
                'title'           => 'Stats With Button',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['stats', 'statistics']
            ]
        );
    }
}