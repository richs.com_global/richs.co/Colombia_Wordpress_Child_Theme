<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark Digital
 * @version  $Revision: 1.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

class TextWithImageCarousel extends Block
{
    public function __construct()
    {
        parent::register_block(
            'text-with-image-carousel',
            [
                'title'           => 'Text With Image Carousel',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['image']
            ]
        );
    }
}