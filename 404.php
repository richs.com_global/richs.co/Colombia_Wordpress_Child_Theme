<?php
/**
 * 404 Page
 */
get_header(); ?>

<img src="/wp-content/uploads/2020/01/404-Banner-Turkey-04.jpg" alt="404 Image" class="banner-404">

<section class="block">
    <div class="container not-found-wrap">
        <div class="txt-center">
            <h1 class="block-title">404</h1>
            <h2 class="block-title">Lo siento, esta página no existe.</h2>
            <p>Por favor revisa la dirección ingresada o vuelve a nuestra página principal y descubre infinitas posibilidades.</p>
            <div class="btn-wrap">
                <a class="btn btn-outline-red" href="<?= site_url('/');?>">VUELVE AL INICIO</a>
            </div>
        </div>

    </div>
</section>

<?php get_footer();


